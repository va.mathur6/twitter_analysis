# DOCUMENTATION

## Summary 
- The overall goal of this code to determine the sentiment of a tweet.
- As of right now, the code is setup to determine the sentiment of at tweet (a written sentence), where the user types a sentence via the terminal. The code will allow the user to consistently submit a sentence, and recieve what the model, that we have come up with, determines the sentiment to be.
- The code is mainly split into two parts. The first part focuses on determining what classifier to use, while the second part is mainly focused on establishing the sentiment of an arbitrary tweet that a user can input using the command line interface.

## PART 1 - Determining the Model to Use
- The first part uses the nltk library.
- We use 4 different models when testing to see which model to use. We use Naive bayes, Bernoulli's, Logistic Regression, and Linear SVC. In our code we automatically find the best model and use that as our classifier. We do this by comparing the accuracies that we calculated when running each of the models and taking the one with the highest value. 
- In the identify_classifier function, the first half is mainly focusing on data cleaning and setting up the data for the training and testing sets. We have to set it up as a dictionary when both testing and running the classifier.

## PART 2 - Determine the Sentiment of the User Tweet
- We pass in the classifier, tweet, accuracy of the model, and word_features (needed to run classify) and we run the tweet in the classifier that we found had the highest accuracy.
- We then print out our result so the the user can see the sentiment. 

## Main Function
- In the main function, we first run the model function and then continously read the user input and print the determined sentiment.
- The user can stop running the code by pressing CTRL-C.

## How to Use the code
- Do the following to run the code
- install git on your computer
- clone the repo locally by running: git clone (https link)
- Install nltk, sklearn and statistics using pip.
- pip3 install statistics
- pip3 install nltk
- pip3 install scikit-learn
- First, uncomment lines 112 and 113 and comment out lines 116-125. The 112th and the 113th lines should read as follows: nltk.download('punkt'), nltk.download(‘averaged_perceptron_tagger’)
- Now, run the program through terminal using the statement: python3 sentiment.py
- Now, comment out lines 112 and lines 113 and uncomment lines 116-125
- Run the python command: python3 sentiment.py

## Team members and Contributions
- Varun - Worked on all aspects of the code. (33.33%)
- Shikhar - Worked primarily on the the classifying aspect and the slides. (33.33%)
- Ishaan - Worked on slides/presentation. (33.33%)