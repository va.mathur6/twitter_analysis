import nltk
import random
from nltk.classify.scikitlearn import SklearnClassifier
import pickle
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC, LinearSVC, NuSVC
from nltk.classify import ClassifierI
from statistics import mode
from nltk.tokenize import word_tokenize

def Naive_Bayes(training_set,testing_set):
    classifier = nltk.NaiveBayesClassifier.train(training_set)
    accuracy = (nltk.classify.accuracy(classifier, testing_set))*100
    return classifier, accuracy

def Bernoulli(training_set,testing_set):
    classifier = SklearnClassifier(BernoulliNB()).train(training_set)
    accuracy = (nltk.classify.accuracy(classifier, testing_set))*100
    return classifier, accuracy

def Logistic_Regression(training_set,testing_set):    
    classifier = SklearnClassifier(LogisticRegression()).train(training_set)
    accuracy = (nltk.classify.accuracy(classifier, testing_set))*100
    return classifier, accuracy

def Linear_SVC(training_set,testing_set):
    classifier = SklearnClassifier(LinearSVC()).train(training_set)
    accuracy = (nltk.classify.accuracy(classifier, testing_set))*100
    return classifier, accuracy

def find_features(document, word_features):
    words = word_tokenize(document)
    features = {}
    for w in word_features:
        features[w] = (w in words)
    return features

def identify_classifier():
    with open("positive.txt", 'r', encoding="latin1") as f1:
        short_pos = f1.read()

        with open("negative.txt", 'r', encoding="latin1") as f2:
            short_neg = f2.read()


            all_words = []
            documents = []

            # We are only using adjectives because are the most helpful for tweet sentiment
            allowed_word_types = ["J"]

            for p in short_pos.split('\n'):
                documents.append( (p, "pos") )
                words = word_tokenize(p)
                pos = nltk.pos_tag(words)
                for w in pos:
                    if w[1][0] in allowed_word_types:
                        all_words.append(w[0].lower())


            for p in short_neg.split('\n'):
                documents.append( (p, "neg") )
                words = word_tokenize(p)
                pos = nltk.pos_tag(words)
                for w in pos:
                    if w[1][0] in allowed_word_types:
                        all_words.append(w[0].lower())

            all_words = nltk.FreqDist(all_words)

            word_features = list(all_words.keys())[:5000]

            featuresets = [(find_features(rev, word_features), category) for (rev, category) in documents]

            random.shuffle(featuresets)

            testing_set = featuresets[8500:]
            training_set = featuresets[:8500]

            max_accuracy = 0
            final_classifier = None

            # After extensive testing we found Bernoulli's is the way to go
            classifier, accuracy =  Naive_Bayes(training_set,testing_set)
            if max_accuracy < accuracy:
                max_accuracy = accuracy
                final_classifier = classifier

            classifier, accuracy =  Logistic_Regression(training_set,testing_set)
            if max_accuracy < accuracy:
                max_accuracy = accuracy
                final_classifier = classifier
            
            classifier, accuracy =  Linear_SVC(training_set,testing_set)
            if max_accuracy < accuracy:
                max_accuracy = accuracy
                final_classifier = classifier
            
            classifier, accuracy =  Bernoulli(training_set,testing_set)
            if max_accuracy < accuracy:
                max_accuracy = accuracy
                final_classifier = classifier
            
            return [final_classifier, accuracy, word_features]

def classify_tweet(classifier, tweet, accuracy, word_features):
    print('We can predict to a ' + str(accuracy) + '% accuracy that the tweet is, ' + classifier.classify(find_features(tweet, word_features)) + '. ')

def main():

    # nltk.download('punkt')
    # nltk.download('averaged_perceptron_tagger')
    
    # Use this to see which classifier to use
    result = identify_classifier()
    classifier, accuracy = result[0], result[1]
    word_features = result[2]
    try:
        while True:
            print('Enter tweet: ')
            user_tweet = input()
            classify_tweet(classifier, user_tweet, accuracy, word_features)
    except KeyboardInterrupt:
        pass

if __name__ == "__main__":
    main()